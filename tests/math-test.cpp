#include "gtest/gtest.h"

#include "cpp_pybind_example/math.h"

TEST(OurMathTest, TestAdd)
{
  ASSERT_EQ(add(1,2), 3);
  ASSERT_EQ(add(2,7), 9);
}

TEST(OurMathTest, TestSubtract)
{
  ASSERT_EQ(subtract(4,2), 2);
  ASSERT_EQ(subtract(6,3), 3);
}
