cmake_minimum_required(VERSION 3.16)

project(cpp_pybind_example
  VERSION 1.0.0
  DESCRIPTION "An example C++ project using pybind11"
  LANGUAGES CXX)

# cmake settings
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_POSITION_INDEPENDENT_CODE ON) # -fPIC
set(CMAKE_EXPORT_COMPILE_COMMANDS ON) # export compile commands needed for some IDEs

# external packages
include(FetchContent)
FetchContent_Declare(
  pybind11
  GIT_REPOSITORY https://github.com/pybind/pybind11
  GIT_TAG        v2.4.3
  )
FetchContent_MakeAvailable(pybind11)
# TODO: add external shared library like MKL

# includes
include_directories(include)

# source code
# TODO: separate python and cpp
# TODO: add namespace
add_subdirectory(src/cpp_pybind_example)

# tests
option(BUILD_TESTING CACHE yes)
if((CMAKE_PROJECT_NAME STREQUAL PROJECT_NAME OR MODERN_CMAKE_BUILD_TESTING) AND BUILD_TESTING)
  enable_testing()
  add_subdirectory(tests)
endif()
