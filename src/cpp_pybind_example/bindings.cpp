#include <pybind11/pybind11.h>
#include "cpp_pybind_example/math.h"

// TODO: fix all the pybind warnings

namespace py = pybind11;

PYBIND11_PLUGIN(cpp_pybind_example) {
  py::module m("cpp_pybind_example", R"doc(
        Python module
        -------------------
        .. currentmodule:: cpp_pybind_example
        .. autosummary::
           :toctree: _generate

           add
           subtract
    )doc");

  m.def("add", &add, R"doc(
        Add two numbers

        Some other information about the add function
    )doc");

  m.def("subtract", &subtract, R"doc(
        Subtract two numbers

        Some other information about the subtract function
    )doc");

  return m.ptr();
}
