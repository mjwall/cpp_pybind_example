add_library(cpp_pybind_example_math math.cpp)
#target_link_libraries(cpp_python_example_math
#                       stdc++fs)

set(PYBIND11_CPP_STANDARD -std=c++11)

# binding stuff
find_package(Python3 REQUIRED)
set(cpp_pybind_example_so_name cpp_pybind_example.cpython-3${Python3_VERSION_MINOR}m-x86_64-linux-gnu.so)
configure_file(setup.py.in ${CMAKE_CURRENT_BINARY_DIR}/setup.py @ONLY)
configure_file(__init__.py ${CMAKE_CURRENT_BINARY_DIR}/cpp_pybind_example/__init__.py COPYONLY)
configure_file(hello.py ${CMAKE_CURRENT_BINARY_DIR}/cpp_pybind_example/hello.py COPYONLY)

pybind11_add_module(cpp_pybind_example MODULE bindings.cpp)
target_link_libraries(cpp_pybind_example
  PRIVATE
  cpp_pybind_example_math)

# TODO: specific python pacakge requirements
# $> pip3 list
#Package    Version
#---------- -------
#auditwheel 3.1.1
#pip        20.1.1
#pyelftools 0.26
#setuptools 39.2.0
#wheel      0.34.2
add_custom_target(build_whl
  #COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_CURRENT_BINARY_DIR}/cpp_pybind_example
  COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_BINARY_DIR}/${cpp_pybind_example_so_name} ${CMAKE_CURRENT_BINARY_DIR}/cpp_pybind_example
  COMMAND ${Python3_EXECUTABLE} setup.py bdist_wheel
	    DEPENDS cpp_pybind_example
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
  )
