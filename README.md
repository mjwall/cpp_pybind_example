# cpp_pybind_example

Example project to help figure out to cpp and pybind interactions.  Initially starting with code from https://www.benjack.io/2018/02/02/python-cpp-revisited.html.  Expanding to create a whl file, then include an external shared library like MKL.

## Discussion

## Usage

### Generate cmake files with 

```
cmake -S. -Bbuild
```

### Build the code with 

```
cmake --build build
```

which will run the default make target, `all`.

### Run cpp tests with 

```
cmake --build build --target test
```

### Test generated python binding manuallye

```
cd build/src/cpp_pybind_example
ls cpp_pybind_example.cpython-36m-x86_64-linux-gnu.so 
python3
>>> import cpp_pybind_example
>>> cpp_pybind_example.add(1,1)
2
>>> cpp_pybind_example.subtract(5, 6)
-1
>>> help(cpp_pybind_example)
Help on module cpp_pybind_example:

NAME
    cpp_pybind_example

DESCRIPTION
    Python module
    -------------------
    .. currentmodule:: cpp_pybind_example
    .. autosummary::
       :toctree: _generate

       add
       subtract

FUNCTIONS
    add(...) method of builtins.PyCapsule instance
        add(arg0: int, arg1: int) -> int


        Add two numbers

        Some other information about the add function

    subtract(...) method of builtins.PyCapsule instance
        subtract(arg0: int, arg1: int) -> int


        Subtract two numbers

        Some other information about the subtract function

FILE
    /home/somedir/cpp_pybind_example/build/src/cpp_pybind_example/cpp_pybind_example.cpython-36m-x86_64-linux-gnu.so                                                           

>>> exit()
```

Note, the hello.say_hello function is not available

### Build the whl file with 

```
cmake --build build --target build_whl
```

### Install the whl file

```
pip3 install --user -I build/src/cpp_pybind_example/dist/cpp_pybind_example-0.0.1-py3-none-any.whl 
```

### Test the installed whl manually

```
python3
>>> import cpp_pybind_example
>>> cpp_pybind_example.add(1,1)
4
>>> help(cpp_pybind_example)
Help on package cpp_pybind_example:

NAME
    cpp_pybind_example

PACKAGE CONTENTS
    cpp_pybind_example
    hello

FUNCTIONS
    add(...) method of builtins.PyCapsule instance
        add(arg0: int, arg1: int) -> int


        Add two numbers

        Some other information about the add function

    subtract(...) method of builtins.PyCapsule instance
        subtract(arg0: int, arg1: int) -> int


        Subtract two numbers

        Some other information about the subtract function

FILE
    /home/$USER/.local/lib/python3.6/site-packages/cpp_pybind_example/__init__.py

```

You can also import the hello module, staying in the python interpretor

```
>>> import cpp_pybind_example.hello
>>> cpp_pybind_example.hello.say_hello()
Hello World!
>>> help(cpp_pybind_example.hello)
Help on module cpp_pybind_example.hello in cpp_pybind_example:

NAME
    cpp_pybind_example.hello

FUNCTIONS
    say_hello()
        Just say hi

FILE
    /home/mjwall/.local/lib/python3.6/site-packages/cpp_pybind_example/hello.py
```

### Remove the whl

```
pip3 uninstall -y cpp_pybind_example
```

### Run automated python tests

```
cd tests
python3 -m venv ../build/cpp_pybind_example
source ../build/cpp_pybind_example/bin/activate
pip3 install ../build/src/cpp_pybind_example/dist/cpp_pybind_example-0.0.1-py3-none-any.whl
python math_tests.py
deactivate
rm -rf ../build/cpp_pybind_example
```

TODO: find a better way here, maybe tox

## Links

- https://www.python.org/dev/peps/pep-0599/ - manylinux2014 for CentOS 7 - does it support C++ 17 though?
- https://docs.python.org/3/reference/import.html 
- https://www.python.org/dev/peps/pep-0427/#installing-a-wheel-distribution-1-0-py32-none-any-whl
- https://stackoverflow.com/questions/27885397/how-do-i-install-a-python-package-with-a-whl-file
- https://python-packaging-tutorial.readthedocs.io/en/latest/setup_py.html
- https://python-packaging-tutorial.readthedocs.io/en/latest/binaries_dependencies.html
- https://github.com/microsoft/onnxruntime/blob/24eda3df3318d86f310918ad9021295d4a5c7898/setup.py
- https://github.com/PaddlePaddle/Paddle/pull/11806/files
- https://pypi.org/project/intel-scipy/#files
- https://bastian.rieck.me/blog/posts/2018/cmake_cpp_pybind11_hard_mode/
- https://www.benjack.io/2018/02/02/python-cpp-revisited.html and https://github.com/benjaminjack/python_cpp_example 
- https://github.com/apache/incubator-mxnet/blob/382279e0d5b88b2bd05e6c00105fe1e9eb2bc600/tools/pip/setup.py
